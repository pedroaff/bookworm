import React from "react";
import LoginForm from "../forms/LoginForm";

class LoginPage extends React.Component {
    _submit = (data) => {
        console.log(data);
    }

  render() {
    return (
      <div>
        <h1>LoginPage Page</h1>
        <LoginForm submit={this._submit} />
      </div>
    );
  }
}

export default LoginPage;
