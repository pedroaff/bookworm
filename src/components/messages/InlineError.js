import React from 'react';
import PropsTypes from 'prop-types';

const InlineError = ({text}) => 
    <span style={{color: 'red'}}>
      {text}
    </span>;

InlineError.propTypes = {
    text: PropsTypes.string.isRequired
};

export default InlineError;